#!/usr/bin/env python3.11

import os, sys, time, logging
try:
    from tqdm import tqdm
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages tqdm")
try:
    import socket
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages sockets")
try:
    import argparse
    from argparse import RawTextHelpFormatter
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages argparse")


parser = argparse.ArgumentParser(description="Port scan", 
                                 formatter_class=RawTextHelpFormatter, 
                                 epilog="Examples:\n"
                                        "./scan_ports.py --ip 40.114.177.156 --port 443 --save scan.txt")
parser.add_argument("--ip", "-i", help="ip address; ex: 40.114.177.156", required=True)
parser.add_argument("--port", "-p", help="""port: 
-p r100       = range 100 ports;
-p r1000      = range 1000 ports;
-p common     = common ports; 
-p all        = all ports;
-p [argv]     = input port""", required=True)
parser.add_argument("--save", "-s", help="save file", required=False)
args = parser.parse_args()


if len(sys.argv) ==7:
    LOG = sys.argv[6]
    logging.basicConfig(level=logging.INFO, filename=LOG, format="%(message)s")
else:
    pass


def specific_port():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.settimeout(0.5)
    code = client.connect_ex((host, int(port)))
    if code == 0:
        print(f"code: {code}");
        print(f"port: {port}");
        print("status: open");
        print("");
        logging.info(f"code: {code}");
        logging.info(f"port: {port}");
        logging.info("status: open");
        logging.info("");
    elif code == 11:
        print(f"code: {code}");
        print(f"port: {port}");
        print("status: closed");
        print("");
        logging.info(f"code: {code}");
        logging.info(f"port: {port}");
        logging.info("status: closed");
        logging.info("");
    elif code == 111:
        print(f"code: {code}");
        print(f"port: {port}");
        print("status: closed");
        print("");
        logging.info(f"code: {code}");
        logging.info(f"port: {port}");
        logging.info("status: closed");
        logging.info("");
    else:
        print(f"code: {code}");
        print(f"port: {port}");
        print("status: undefined");
        print("");
        logging.info(f"code: {code}");
        logging.info(f"port: {port}");
        logging.info("status: undefined");
        logging.info("");


def range_100_ports():
    ports = range(1,100,1);
    for port in tqdm(ports):
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.settimeout(0.5)
        code = client.connect_ex((host, port))
        if code == 0:
            print(f"code: {code}");
            print(f"port: {port}");
            print("status: open");
            print("");
            logging.info(f"code: {code}");
            logging.info(f"port: {port}");
            logging.info("status: open");
            logging.info("");
        elif code == 11:
            print(f"code: {code}");
            print(f"port: {port}");
            print("status: closed");
            print("");
            logging.info(f"code: {code}");
            logging.info(f"port: {port}");
            logging.info("status: closed");
            logging.info("");
        elif code == 111:
            print(f"code: {code}");
            print(f"port: {port}");
            print("status: closed");
            print("");
            logging.info(f"code: {code}");
            logging.info(f"port: {port}");
            logging.info("status: closed");
            logging.info("");
        else:
            print(f"code: {code}");
            print(f"port: {port}");
            print("status: undefined");
            print("");
            logging.info(f"code: {code}");
            logging.info(f"port: {port}");
            logging.info("status: undefined");
            logging.info("");


def range_1000_ports():
    ports = range(1,1000,1);
    for port in tqdm(ports):
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.settimeout(0.5)
        code = client.connect_ex((host, port))
        if code == 0:
            print(f"code: {code}");
            print(f"port: {port}");
            print("status: open");
            print("");
            logging.info(f"code: {code}");
            logging.info(f"port: {port}");
            logging.info("status: open");
            logging.info("");
        elif code == 11:
            print(f"code: {code}");
            print(f"port: {port}");
            print("status: closed");
            print("");
            logging.info(f"code: {code}");
            logging.info(f"port: {port}");
            logging.info("status: closed");
            logging.info("")
        elif code == 111:
            print(f"code: {code}")
            print(f"port: {port}")
            print("status: closed")
            print("")
            logging.info(f"code: {code}")
            logging.info(f"port: {port}")
            logging.info("status: closed")
            logging.info("")
        else:
            print(f"code: {code}")
            print(f"port: {port}")
            print("status: undefined")
            print("")
            logging.info(f"code: {code}")
            logging.info(f"port: {port}")
            logging.info("status: undefined")
            logging.info("")


def all_ports():
    ports = range(1,65533,1)
    for port in tqdm(ports):
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.settimeout(0.5)
        code = client.connect_ex((host, port))
        if code == 0:
            print(f"code: {code}")
            print(f"port: {port}")
            print("status: open")
            print("")
            logging.info(f"code: {code}")
            logging.info(f"port: {port}")
            logging.info("status: open")
            logging.info("")
        elif code == 11:
            print(f"code: {code}")
            print(f"port: {port}")
            print("status: closed")
            print("")
            logging.info(f"code: {code}")
            logging.info(f"port: {port}")
            logging.info("status: closed")
            logging.info("")
        elif code == 111:
            print(f"code: {code}")
            print(f"port: {port}")
            print("status: closed")
            print("")
            logging.info(f"code: {code}")
            logging.info(f"port: {port}")
            logging.info("status: closed")
            logging.info("")
        else:
            print(f"code: {code}")
            print(f"port: {port}")
            print("status: undefined")
            print("")
            logging.info(f"code: {code}")
            logging.info(f"port: {port}")
            logging.info("status: undefined")
            logging.info("")


def common_ports():
    ports = [20, 21, 22, 53, 69, 80, 135, 137, 138, 139, 443, 445, 989, 990, 3124, 3128, 3306, 5353, 8080]
    for port in tqdm(ports):
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.settimeout(0.5)
        code = client.connect_ex((host, port))
        if code == 0:
            print(f"code: {code}")
            print(f"port: {port}")
            print("status: open")
            print("")
            logging.info(f"code: {code}")
            logging.info(f"port: {port}")
            logging.info("status: open")
            logging.info("")
        elif code == 11:
            print(f"code: {code}")
            print(f"port: {port}")
            print("status: closed")
            print("")
            logging.info(f"code: {code}")
            logging.info(f"port: {port}")
            logging.info("status: closed")
            logging.info("")
        elif code == 111:
            print(f"code: {code}")
            print(f"port: {port}")
            print("status: closed")
            print("")
            logging.info(f"code: {code}")
            logging.info(f"port: {port}")
            logging.info("status: closed")
            logging.info("")
        else:
            print(f"code: {code}")
            print(f"port: {port}")
            print("status: undefined")
            print("")
            logging.info(f"code: {code}")
            logging.info(f"port: {port}")
            logging.info("status: undefined")
            logging.info("")


if __name__ == "__main__":
    try:
        host = sys.argv[2]
        op = str(sys.argv[3])
        if op == "-p" or "--port":
            try:
                if type(int(sys.argv[4])) ==int:
                    port = sys.argv[4];
                    specific_port()
            except:
                if sys.argv[4] == "r100":
                    range_100_ports()

                elif sys.argv[4] == "r1000":
                    range_1000_ports()

                elif sys.argv[4] == "common":
                    common_ports()

                elif sys.argv[4] == "all":
                    all_ports()

                else:
                    sys.exit()

    except KeyboardInterrupt:
        sys.exit()
